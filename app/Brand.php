<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model 
{
    public function category(){
       return $this->belongsToMany(Category::class,'category_brand','brand_id','category_id');
    }
    public function subCategory(){
       return $this->belongsToMany(Category::class,'subCategory_brand','brand_id','subCategory_id');
    }
    public function product(){
        return $this->hasMany(Product::class);
    }
}

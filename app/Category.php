<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model//user
{
    public function brand(){
       return $this->belongsToMany(Brand::class,'category_brand','category_id', 'brand_id');
    }
    public function product(){
        return $this->hasMany(Product::class);
    }

    public function subCategory(){
        return $this->hasMany(Sub_category::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    public function product_deatil(){
       return $this->hasMany(Product_deatils::class);
    }
    public function product_color(){
       return $this->hasMany(Product_color::class);
    }
    public function product(){
       return $this->belongsToMany(Product::class,'product_size', 'size_id','product_id');
    }
}

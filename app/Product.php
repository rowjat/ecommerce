<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function color(){
       return $this->belongsToMany(Color::class,'product_colors','product_id', 'color_id');
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function subCategory(){
        return $this->belongsTo(Sub_category::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function pictur(){
        return $this->hasMany(pictur::class);
    }
    public function product_detail(){
        return $this->hasMany(Product_deatils::class);
    }
   public function size(){
       return $this->belongsToMany(Size::class,'product_size','product_id', 'size_id');
    }
    
    public static function hasProductDetail($product,$color='',$size=''){
        if(empty($color)){
            $color = 1;
        }
        if(empty($size)){
            $size = 1;
        }
        $product_color = Product_color::where('product_id',$product)->where('color_id',$color)->first();
        
        $product_deatils = Product_deatils::where('product_color_id',$product_color->id)->where('size_id',$size)->first();
       return $product_deatils?$product_deatils:false;
    }
}

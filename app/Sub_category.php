<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_category extends Model
{
    public function category(){
        return $this->belongsTo(Category::class);
    }
     public function product(){
        return $this->hasMany(Product::class);
    }
    public function brand(){
       return $this->belongsToMany(Brand::class,'subCategory_brand','subCategory_id', 'brand_id');
    }
}

<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Illuminate\Auth\Authenticatable as AuthenticableTrait;
class User extends Authenticatable
{
    use Notifiable;
//    use AuthenticableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function customer(){
        return $this->hasMany(Customer::class);
    }

    public function role(){
        return $this->belongsToMany(Role::class,'user_role','user_id','role_id');
    }
    
    public function hasAnyRole($roles){
        if(is_array($roles)){
            foreach ($roles as $role){
                if($this->hasRole($role)){
                    return true;
                }
            }
        } else {
            if($this->hasRole($roles)){
                    return true;
                }
        }
        return FALSE;
    }
    
    public function hasRole($role){
        if($this->role()->where('name',$role)->first()){
            return true;
        }
        return false;
    }
    
}

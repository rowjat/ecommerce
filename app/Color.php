<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    public function product(){
       return $this->belongsToMany(Product::class,'product_colors','color_id', 'product_id');
    }
}

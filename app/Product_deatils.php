<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_deatils extends Model
{
    public function product_color(){
        return $this->belongsTo(Product_color::class);
    }
    public function size(){
        return $this->belongsTo(Size::class);
    }
    public function product(){
        return $this->belongsTo(Product::class);
    }
    
}

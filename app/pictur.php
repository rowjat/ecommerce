<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product_deatils;

class pictur extends Model {

    public function product_color() {
        return $this->belongsToMany(Product_color::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public static function hasPictur($oroduct_detail_id) {
        return parent::where('product_id', Product_deatils::find($oroduct_detail_id)->product_id)->get();
    }

}

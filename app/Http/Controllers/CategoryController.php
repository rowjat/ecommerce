<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Sub_category;
use App\Brand;
use Session;

class CategoryController extends Controller {

    public function getCategory() {
        $categories = Category::paginate(8);
        $brands = Brand::where('states', 1)->get();
        return view('admin.category.getCategory', compact(['categories', 'brands']));
    }

    public function getSubcategory() {
        $categories = Category::where('states', 1)->get();
        $subCategories = Sub_category::paginate(8);
        return view('admin.category.getSubcategory', compact(['categories', 'subCategories']));
    }
    public function getBrand(Request $request){
        if($request->ajax()){
           return Category::find($request->id)->brand;
        }
    }

    public function createCategory(Request $request) {

        $this->validate($request, [
            'name' => 'required|unique:categories'
        ]);
        $table = new Category();
                $table->name = $request->name;
                if ($request->states) {
                    $table->states = $request->states;
                }
                $table->save();
        if ($request->brand > 0) {
            for ($i=0; $i < count($request->brand); $i++) {
                $table->brand()->attach($request->brand[$i]);
            }
        }

        Session::flash('success', 'Category has been save successfull');
        return redirect()->back();
    }

    public function createSubCategory(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:sub_categories'
        ]);
        $table = new Sub_category();
        $table->name = $request->name;
        $table->category_id = $request->category_id;
        if ($request->states) {
            $table->states = $request->states;
        }
        $table->save();
        if ($request->brand > 0) {
            for ($i=0; $i < count($request->brand); $i++) {
                $table->brand()->attach($request->brand[$i]);
            }
        }
        Session::flash('success', 'SubCatagery has been save successfull');
        return redirect()->back();
    }

}

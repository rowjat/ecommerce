<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_deatils;
use App\Stock_in;
use Session;
class StockInController extends Controller
{
    public function stockIn(Request $request){
         $this->validate($request, [
            'date'=>'required',
             'supplier_id'=>'required',
              'challana_no'=>'required',
            'supplier_price'=>'required',
            'product_details_id.*'=>'required',
              'qty.*'=>'required'
        ]);
       $request_count = count($request->product_details_id);
        if($request_count >0){
            for($i = 0;$i < $request_count;$i++){
                $product_details = Product_deatils::find($request->product_details_id[$i]);
                $product_details->increment('qty',$request->qty[$i]);
                $product_details->save();
                
                $stock_in = new Stock_in();
                $stock_in->date = $request->date;
                $stock_in->supplier_id = $request->supplier_id;
                $stock_in->challana_no = $request->challana_no;
                $stock_in->supplier_price = $request->supplier_price;
                $stock_in->qty = $request->qty[$i];
                $stock_in->product_details_id = $request->product_details_id[$i];
                $stock_in->save();
                
            }
            Session::flash('success','Data save successfull');
            return redirect()->back();
        }
    }
}

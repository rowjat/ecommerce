<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Customer;
use Illuminate\Support\Facades\Auth;
use Session;
use Cart;

class CustomerController extends Controller {

    public function create(){
        return view('customer.create');
    }

    public function register(Request $request) {

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->role()->attach(Role::where('name', 'customer')->first());

        $customer = new Customer();
        $customer->user_id = $user->id;
        $customer->save();
        Auth::login($user);
        if (Cart::count() > 0) {
            $carts = Cart::content();
            return view('customer.check_out', compact('carts'));
        } else {
            return redirect()->to('/');
        }
    }

    public function login(Request $request) {
        $this->validate($request, [
            'email_1' => 'required|email|max:255',
            'password_1' => 'required|min:6'
        ]);
        if (Auth::check()) {
            session()->flash('success', 'User All ready login');
            return redirect()->back();
        } else {
            if(User::where('email',$request->email_1)->first()->role()->where('name','customer')->first()){
                if(Auth::attempt(['email' => $request->email_1, 'password' => $request->password_1])) {
               
                if ($carts->count() > 0) {
                    session()->flash('success', 'login');
                    return redirect()->to('/order/checkout');
                } else {
                    session()->flash('success', 'login');
                    return redirect()->to('/');
                }
            } else {
                session()->flash('success', 'Email or Password Don\'t Match');
                return redirect()->back();
            }
            }
            
        }
    }
    public function logout(){
        if(Auth::check()){
            Auth::logout();
           return redirect()->to('/');
        } else {
           return redirect()->to('/');
        }
        
        
    }

}

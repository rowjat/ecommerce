<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
//    public function __construct() {
//        $this->middleware(['auth','roles']);
//    }


    public function index(){
        
      if(auth()->check() && auth()->user()->hasAnyRole(['admin','super_admin'])){
          return view('admin.index');
      } else {
         return view('admin.auth.login'); 
      }
//        
        
    }
}

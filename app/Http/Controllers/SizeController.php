<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Size;
use Session;

class SizeController extends Controller
{
    public function index(){
        $sizes = Size::paginate(8);
        return view('admin.size.index', compact('sizes'));
    }
    
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|unique:sizes',
        ]);
        $table = new Size();
        $table->name = $request->name;
        $table->save();
        Session::flash('success','Size Have been save successfull');
        return redirect()->back();
    }
}

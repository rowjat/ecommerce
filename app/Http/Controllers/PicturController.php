<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_color;
use App\Product;
use App\pictur;
use App\Color;
use Session;

class PicturController extends Controller {

    public function index($id) {
        $product_colors = Product_color::where('product_id', $id)->get();
        $products = Product::find($id);
        return view('admin.pictur.index', compact('product_colors', 'products'));
    }


    public function store(Request $request) {
        $this->validate($request, [
            'pictur.*' => 'required|image|mimes:jpg,jpeg,bmp,png'
        ]);
        if (count($request->file('pictur')) > 0) {
            for ($i = 0; $i < count($request->file('pictur')); $i++) {
                $path = 'asset\admin\images\product';
                $file_name = rand(11111, 99999) . '_' . time() . '_' . $request->id . '.' . $request->file('pictur')[$i]->getClientOriginalExtension();
                $request->file('pictur')[$i]->move($path, $file_name);

                $table = new \App\pictur();
                $table->product_color_id = $request->id;
                $table->product_id = $request->product_id;
                $table->pictur_name = $file_name;

                if (\App\pictur::where('product_id', $request->product_id)->where('states', 1)->get()->count() == 0) {//1 t
                    $table->states = 1;
                }
                $table->save();
            }
            Session::flash('success', 'Pictur Upload Successfull');
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function viewPictur(Request $request) {
        if ($request->ajax()) {
            $product_color = Product_color::find($request->id)->color_id;
            $product_color = Color::find($product_color);
            $datas = Pictur::where('product_color_id', $request->id)->get();
            return view('admin.pictur.show', compact(['datas', 'product_color']));
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Color;
use App\Size;
use App\Sub_category;
use App\Brand;
use App\Product_color;
use App\Product_deatils;
use App\pictur;
use App\Supplier;
use Session;

class ProductController extends Controller {

    public function index() {
        $products = Product::paginate(10);
        return view('admin.product.index', compact('products'));
    }

    public function create() {
        $categories = Category::where('states', 1)->get();
        $colors = Color::all();
        $sizes = Size::all();
        return view('admin.product.create', compact(['categories', 'colors', 'sizes']));
    }

    public function ajaxAction(Request $requist) {
        if ($requist->ajax()) {
            if ($requist['i'] = 'category') {
                $subCategories = 1;
                if (Sub_category::where('states', 1)->where('category_id', $requist['id'])->count() > 0) {
                    $subCategories = Sub_category::where('states', 1)->where('category_id', $requist['id'])->get();
                }
                $brand = Category::find($requist['id'])->brand()->get();
                return $data = ['subCategories' => $subCategories, 'brand' => $brand];
            }
        }
    }

    public function store(Request $request) {
//        return $request->all();
        $this->validate($request, [
            'category_id' => 'required',
//            'sub_category_id' => $request->sub_category_id?'required':'',
            'brand_id' => 'required',
            'code' => 'required|unique:products',
            'name' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'model_no' => 'required|unique:products',
            'qty_per_unit' => 'required',
            'old_unit_price' => 'required',
            'unit_price' => 'required'
//            'color_id' => 'required',
//            'size_id' => 'required'
        ]);
        $table = new Product();
        $table->category_id = $request->category_id;
        $table->sub_category_id = $request->sub_category_id ? $request->sub_category_id : 0;
        $table->brand_id = $request->brand_id;
        $table->code = $request->code;
        $table->name = $request->name;
        $table->short_description = $request->short_description;
        $table->long_description = $request->long_description;
        $table->model_no = $request->model_no;
        $table->qty_per_unit = $request->qty_per_unit;
        $table->old_unit_price = $request->old_unit_price;
        $table->unit_price = $request->unit_price;
        if ($request->states) {
            $table->states = $request->states;
        }
        $table->save();
        if ($request->color_id) {
            for ($i = 0; $i < count($request->color_id); $i++) {
                $color = $table->color()->attach($request->color_id[$i]);
            }
        } else {
            $product_color = new Product_color();
            $product_color->product_id = $table->id;
            $product_color->color_id = 1;
            $product_color->save();
        }
        if ($request->size_id) {
            for ($i = 0; $i < count($request->size_id); $i++) {
                $size = $table->size()->attach($request->size_id[$i]);
            }
        } else {
             $size = $table->size()->attach(1);
        }
        $product = Product_color::where('product_id', $table->id)->get();
        foreach ($product as $value) {
            if ($request->size_id) {
                for ($i = 0; $i < count($request->size_id); $i++) {
                    $product_deatils = new Product_deatils();
                    $product_deatils->product_id = $table->id;
                    $product_deatils->product_color_id = $value->id;
                    $product_deatils->size_id = $request->size_id[$i];
                    $product_deatils->save();
                }
            } else {
                    $product_deatils = new Product_deatils();
                    $product_deatils->product_id = $table->id;
                    $product_deatils->product_color_id = $value->id;
                    $product_deatils->size_id = 1;
                    $product_deatils->save();
            }
        }
        Session::flash('success', 'Product have been save successfull');
        return redirect()->back();
    }

    public function productView($id) {
        $suppliars = Supplier::all();
        $products = Product_color::where('product_id', $id)->get();
        return view('admin.product_deatils.index', compact(['products', 'suppliars']));
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Shipping;
use Auth;
use App\Order;
use App\Order_detail;
use App\Product;
use Stripe\Stripe;
use Stripe\Charge;

class ChackOutController extends Controller {

    public function checkOut() {
        if (auth()->check() && auth()->user()->hasAnyRole(['customer'])) {
            $carts = Cart::content();
            return view('customer.check_out', compact('carts'));
        } else {
            if(auth()->check()){
                auth()->logout();
            }
            return redirect()->to('/customer/create');
        }
    }

    public function order(Request $request) {
        $this->validate($request, [
            'mobile' => 'required',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
            'address' => 'required',
            'payment_type' => 'required'
        ]);
        $shipping = new Shipping();
        $shipping->customer_id = Auth::user()->customer()->first()->id;
        $shipping->mobile = $request->mobile;
        $shipping->country = $request->country;
        $shipping->city = $request->city;
        $shipping->state = $request->state;
        $shipping->address = $request->address;
        $shipping->save();
        if ($request->payment_type == 1) {
            $order = new Order();
            $order->customer_id = Auth::user()->customer()->first()->id;
            $order->payment_type = $request->payment_type;
            $order->shipping_id = $shipping->id;
            $order->states = 1; //1=paending,2=confirm, 3=delivari,4=cancle,
            $order->save();
            $carts = Cart::content();
            foreach ($carts as $cart) {
                $order_detail = new Order_detail();
                $order_detail->order_id = $order->id;
                $order_detail->product_detail_id = $cart->id;
                $order_detail->qty = $cart->qty;
                $order_detail->unit_price = $cart->price;
                $order_detail->save();
            }
            Cart::destroy();
            return redirect()->to('/');
        } elseif ($request->payment_type == 2) {
            //paypal
            return redirect()->to('payment/'.$shipping->id);
        }
    }

    public function payment($id) {
        
        return view('pages.payment')->withId($id);
    }

    public function store(Request $request,$id) {
        if (Cart::count() > 0) {
            Stripe::setApiKey('sk_test_wl4IyoWmSjeC5uRrCJ5qPtK1');
            try {
                Charge::create([
                    "amount" => Cart::total() * 100,
                    "currency" => "usd",
                    "source" => $request->stripeToken, // obtained with Stripe.js
                    "description" => "Charge for david.williams@example.com"
                ]);
            } catch (\Exception $ex) {
                return redirect()->back()->with('error', $ex->getMessage());
            }
            $order = new Order();
            $order->customer_id = Auth::user()->customer()->first()->id;
            $order->payment_type = 2;
            $order->shipping_id = $id;
            $order->states = 2; //1=paending,2=confirm, 3=delivari,4=cancle,
            $order->save();
            $carts = Cart::content();
            foreach ($carts as $cart) {
                $order_detail = new Order_detail();
                $order_detail->order_id = $order->id;
                $order_detail->product_detail_id = $cart->id;
                $order_detail->qty = $cart->qty;
                $order_detail->unit_price = $cart->price;
                $order_detail->save();
            }
            Cart::destroy();
            return redirect()->to('/');
        }
        return redirect()->to('/');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class StatesController extends Controller
{
    public function states(Request $request){
         $this->hasStates($request->id,$request->states,$request->table);
         return redirect()->back();
    }
    
    private function hasStates($id,$states,$table){
       return $data = DB::table($table)
            ->where('id', $id)
            ->update(['states' => $states]);
    return DB::table($table)
            ->where('id', $id)->first()->states;
    }
}

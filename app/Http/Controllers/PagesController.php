<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Brand;
use App\Product;
use App\pictur;
use Cart;

class PagesController extends Controller {

    public function home() {
        $categories = Category::where('states', 1)->get();
        $brands = Brand::where('states', 1)->get();
        $sidebar = view('partial._sidebar', compact(['categories', 'brands']));
        $products = Product::where('states', 1)->get();
        return view('pages.home', compact(['sidebar', 'products', 'categories']));
    }

    public function product_details($id) {
        $categories = Category::where('states', 1)->get();
        $brands = Brand::where('states', 1)->get();
        $sidebar = view('partial._sidebar', compact(['categories', 'brands']));
        $product = Product::find($id);
        return view('pages.product_details', compact(['sidebar', 'product']));
    }

    public function shop(Request $request) {
        $categories = Category::where('states', 1)->get();
        $brands = Brand::where('states', 1)->get();
        $sidebar = view('partial._sidebar', compact(['categories', 'brands']));

        $products = Product::where('states', 1)->get();
        if ($request->cat) {
            $products = Product::where('states', 1)
                    ->where('category_id', $request->cat)
                    ->get();
        } elseif ($request->subCat) {
            $products = Product::where('states', 1)
                    ->where('sub_category_id', $request->subCat)
                    ->get();
        } elseif ($request->brand) {
            $products = Product::where('states', 1)
                    ->where('brand_id', $request->brand)
                    ->get();
        }
        return view('pages.shop', compact(['sidebar', 'products']));
    }

    public function pictureView(Request $request) {
        if ($request->ajax()) {
            $pictur = pictur::find($request->id)->pictur_name;
            return view('pages.picturView')->withPictur($pictur);
        }
    }

    public function qtyView(Request $request) {
        if ($request->ajax()) {
            $color = $request->color_id;
            $size = $request->size_id;
            if($request->input == 1){
                 return Product::hasProductDetail($request->product_id, $color, $size)->qty;
            }   
        }
    }

}

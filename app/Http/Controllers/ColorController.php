<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Color;
use Session;

class ColorController extends Controller
{
    public function index(){
        $colors = Color::paginate(8);
        return view('admin.color.index', compact('colors'));
    }
    
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required'
        ]);
        $table = new Color();
        $table->name = $request->name;
        $table->code = $request->code;
        $table->save();
        Session::flash('success','Color have been save successfull');
        return redirect()->back();
               
    }
}

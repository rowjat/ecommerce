<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product_color;

class CartController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $carts = Cart::content();
        return view('pages.cart', compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        return $request->all();
        $price = \App\Product::find($request->product_id)->unit_price;
        $color = $request->color_id?$request->color_id:'';
        $size = $request->size_id?$request->size_id:'';

       $product = \App\Product::hasProductDetail($request->product_id, $color, $size);
        Cart::add(['id' => $product->id, 'name' => $product->product->name, 'qty' => $request->qty, 'price' => $price, 'options' => ['size' => $request->size_id?$request->size_id:1,'color' =>$request->color_id?$request->color_id:1]]);
//        Cart::destroy();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id) {
     
       Cart::update($id, $request->quantity);
       return redirect()->back();
    }
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        Cart::remove($id);
        return redirect()->back();
    }

}

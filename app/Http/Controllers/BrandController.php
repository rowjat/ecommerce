<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Session;

class BrandController extends Controller {

    public function index() {
        $brands = Brand::paginate(8);
        return view('admin.brand.index', compact('brands'));
    }

    public function create(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:brands',
            'description' => 'required',
            'logo' => 'required|image|mimes:jpeg,bmp,png|max:1000'
        ]);
        if ($request->file('logo')) {
            $fileName = 'brand_logo_' . rand(11111, 99999) . '.' . $request->file('logo')->getClientOriginalExtension();
            $destinationPath = 'asset\admin\images\brand';
            $request->file('logo')->move($destinationPath, $fileName);

            $table = new Brand();
            $table->name = $request->name;
            $table->description = $request->description;
            $table->logo = $fileName;
            if ($request->states) {
                $table->states = $request->states;
            }
            $table->save();
            Session::flash('success','Brand have been save successfull');
            return redirect()->back();
        }    
    }
    

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use Session;
class SupplierController extends Controller
{
    public function index(){
        return view('admin.supplier.index');
    }
    public function store(Request $request){
        $this->validate($request, [
            'company_name'=>'required',
            'address'=>'required',
            'city'=>'required',
            'state'=>'required',
            'contry'=>'required',
            'phone'=>'required',
            'email'=>'required|email|unique:suppliers'
        ]);
        $table = new Supplier();
        $table->company_name = $request->company_name;
        $table->address = $request->address;
        $table->city = $request->city;
        $table->state = $request->state;
        $table->contry = $request->contry;
        $table->phone = $request->phone;
        $table->email = $request->email;
        $table->save();
        Session::flash('success','Data Has been save successfull');
        return redirect()->to('/supplier');
    }
    public function create(){
        return view('admin.supplier.create');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Session;


class RegisterController extends Controller
{
    public function getRegister(){
        return view('admin.auth.register');
    }

    public function register(Request $request) {

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);
//        return $request->all();
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->role()->attach(Role::where('name', 'author')->first());
        
        
        Auth::login($user);
        session()->flash('success','login Successfull');
        return redirect()->to('/');
    }
        public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6'
        ]);
        if (Auth::check()) {
            session()->flash('success', 'User All ready login');
            return redirect()->back();
        } else {
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
               if(auth()->check() && auth()->user()->hasAnyRole(['admin','super_admin'])){
                   session()->flash('success','Admin Success full login');
                   return redirect()->to('/admin/2017');
               } else {
                   auth()->logout();
                   session()->flash('success','Not Admin User');
                 return redirect()->to('/admin/2017');
               }
                
            } else {
                session()->flash('success', 'Email or Password Don\'t Match');
                return redirect()->to('/admin/2017');
            }
        }
    }
}

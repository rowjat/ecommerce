<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::get('/admin/2017', 'AdminController@index');
Route::get('/auth/register', 'Auth\RegisterController@getRegister');
Route::post('/auth/register', 'Auth\RegisterController@register');
Route::post('/auth/login', 'Auth\RegisterController@login');
Route::group(['middleware' => 'roles', 'roles' => ['super_admin', 'admin']], function () {
    Route::get('/category', 'CategoryController@getCategory');
    Route::POST('/category_store', 'CategoryController@createCategory');
    Route::get('/subcategory', 'CategoryController@getSubcategory');
    Route::POST('/subcategory_store', 'CategoryController@createSubCategory');
    Route::POST('/subcategory/brand', 'CategoryController@getBrand');


    Route::get('/brand', 'BrandController@index');
    Route::POST('/brand_store', 'BrandController@create');

    Route::get('/product', 'ProductController@index');
    Route::get('/product.create', 'ProductController@create');
    Route::POST('/product_store', 'ProductController@store');
    Route::get('/product/{id}', 'ProductController@productView');

    Route::POST('/product_ajaxAction', 'ProductController@ajaxAction');

    Route::get('supplier', 'SupplierController@index');
    Route::POST('supplier_store', 'SupplierController@store');
    Route::get('supplier_create', 'SupplierController@create');

    Route::Post('/stock_in', 'StockInController@stockIn');


    Route::get('/color', 'ColorController@index');
    Route::POST('/color_store', 'ColorController@store');

    Route::get('/size', 'SizeController@index');
    Route::POST('/size_store', 'SizeController@store');

    Route::PUT('/states', 'StatesController@states');

    Route::get('/pictur/{id}', 'PicturController@index');
    Route::post('/pictur.store', 'PicturController@store');
    Route::post('/pictur_viewPictur', 'PicturController@viewPictur');
});
Route::post('/customer/register', 'CustomerController@register');
Route::post('/customer/login', 'CustomerController@login');
Route::get('/customer/logout', 'CustomerController@logout');
Route::get('/customer/create', 'CustomerController@create');




Route::get('/order/checkout', 'ChackOutController@checkOut');
Route::POST('/order', 'ChackOutController@order');
Route::get('/payment/{id}', 'ChackOutController@payment');
Route::POST('/payment/store/{id}', 'ChackOutController@store');



Route::get('/', 'PagesController@home');
Route::get('/details/{id}', 'PagesController@product_details');
Route::get('/shop', 'PagesController@shop');
Route::POST('/picture/view', 'PagesController@pictureView');
Route::POST('/qty/view', 'PagesController@qtyView');

Route::resource('/cart', 'CartController');


@extends('admin.master')
@section('breadcrumb')
<li class=""><a href="">Dashboard</a></li>
<li class="active">Category</li>
@endsection
@section('page-header')
Category
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Category 
    <?php
    echo class_basename(app('request')->route()->getAction()['controller']);
    ?>
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-7">
        <table id="simple-table" class="table  table-hover panel panel-default">
            <thead>
                <tr>
                    <th class="detail-col">Category Name</th>
                    <th class="">Status</th>

                    <th style="text-align: right">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($categories as $category)
                <tr>
                    <td contenteditable="">{{$category->name}}</td>
                    <td>
                        {!! Form::open(['method'=>'PUT','url'=>'/states']) !!}
                        @php($item = $category)
                        <input type="hidden" name="table" value="categories">
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <input type="hidden" name="states" value="{{$item->states==1?0:1}}">
                        <button type="submit" class="btn btn-xs {{$item->states==1?'btn-success':'btn-danger'}}">
                            <i class="ace-icon fa {{$item->states==1?'fa-check':'fa-close'}} bigger-120"></i>
                        </button>&nbsp;&nbsp;{{$item->states==1?'Published':'Unpublished'}}
                        {!! Form::close() !!}
                    </td>

                    <td style="text-align: right" >
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-success {{$category->states==1?'btn-success':'btn-danger'}} ">
                                <i class="ace-icon fa  {{$category->states==1?'fa-check':'fa-remove'}} bigger-120" data-rel="tooltip" title="{{$category->states==1?'Published':'Unpublished'}}"></i>
                            </button>
                            <button class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>

                            <button class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        @if($category->states==1)
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Published">
                                            <span class="green">
                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                            </span>
                                        </a>
                                        @else
                                        <a href="#" class="tooltip-error" data-rel="tooltip" title="UnPublished">
                                            <span class="red">
                                                <i class="ace-icon fa fa-remove bigger-120"></i>
                                            </span>
                                        </a>
                                        @endif
                                    </li>
                                    <li>
                                        <a href="#" class="tooltip-info" data-rel="tooltip" title="Edit">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                                            <span class="green">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        <div>
            {!! $categories->render() !!}
        </div>



    </div>
    <div class="col-xs-12 col-sm-offset-1 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Category
            </div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','url'=>'/category_store']) !!}
                <input type="text" name="name" id="name" placeholder="Category Name" class="form-control" />
                <!--<label class="control-label">Sub Category :</label>-->
                <br/>
                <label class="control-label" for="brand">Select Brand</label>
                <select class="form-control" id="brand" name="brand[]" id="brand"  multiple="">

                    @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                    @endforeach
                </select>

                <br/>
                <label>
                    <input type="checkbox" name="states" value="1" id="id-file-format" class="ace" />
                    <span class="lbl"> Published</span>
                </label>
                <br/>
                <br/>
                <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Reset
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>


@endsection
@section('js')
<script type="text/javascript">

    $(document).ready(function () {
        $("#brand").chosen({
            disable_search_threshold: 10
        });
    });
</script>
@endsection

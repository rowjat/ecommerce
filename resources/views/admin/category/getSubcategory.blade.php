@extends('admin.master')
@section('breadcrumb')
<li class="active">Dashboard</li>
@endsection
@section('page-header')
Sub Category 
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Sub Category 
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-7">
        <table id="simple-table" class="table  table-hover panel panel-default">
            <thead>
                <tr>
                    <th class="detail-col">Category Name</th>
                    <th class="">Status</th>

                    <th style="text-align: right">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($subCategories as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td>
                        {!! Form::open(['method'=>'PUT','url'=>'/states']) !!}
                        @php($item = $category)
                        <input type="hidden" name="table" value="sub_categories">
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <input type="hidden" name="states" value="{{$item->states==1?0:1}}">
                        <button type="submit" class="btn btn-xs {{$item->states==1?'btn-success':'btn-danger'}}">
                            <i class="ace-icon fa {{$item->states==1?'fa-check':'fa-close'}} bigger-120"></i>
                        </button>&nbsp;&nbsp;{{$item->states==1?'Published':'Unpublished'}}
                        {!! Form::close() !!}
                    </td>

                    <td style="text-align: right">
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-success {{$category->states==1?'btn-success':'btn-danger'}} ">
                                <i class="ace-icon fa  {{$category->states==1?'fa-check':'fa-remove'}} bigger-120" data-rel="tooltip" title="{{$category->states==1?'Published':'Unpublished'}}"></i>
                            </button>
                            <button class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>

                            <button class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                    <li>
                                        @if($category->states==1)
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Published">
                                            <span class="green">
                                                <i class="ace-icon fa fa-check bigger-120"></i>
                                            </span>
                                        </a>
                                        @else
                                        <a href="#" class="tooltip-error" data-rel="tooltip" title="UnPublished">
                                            <span class="red">
                                                <i class="ace-icon fa fa-remove bigger-120"></i>
                                            </span>
                                        </a>
                                        @endif
                                    </li>
                                    <li>
                                        <a href="#" class="tooltip-info" data-rel="tooltip" title="Edit">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                                            <span class="green">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        <div>
            {!! $subCategories->render() !!}
        </div>



    </div>
    <div class="col-xs-12 col-sm-offset-1 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Category
            </div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','url'=>'/subcategory_store']) !!}
                <input type="text" name="name" class="form-control" id="name" placeholder="Category Name"  />
                <!--<label class="control-label">Sub Category :</label>-->
                <br/>
                <select name="category_id" class="form-control" id="category" >
                    <option value="">Select Sub_category</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" style="font-weight: bold">{{$category->name}}</option>
                    @endforeach
                </select>
                <br/>
                <select name="brand[]" size="2" tabindex="2" class="form-control" id="brand_id" multiple="" data-placeholder='sookk'>

                </select>
                <br/>
                <br/>
                <label>
                    <input type="checkbox" name="states" value="1" id="id-file-format" class="ace" />
                    <span class="lbl"> Published</span>
                </label>
                <br/>
               
                <br/>
                <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Reset
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>


@endsection

@section('js')
<script>
 
    $(document).ready(function () {
//        $("#brand_id").chosen({
//            no_results_text: "Oops, nothing found!"
//        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('change', '#category', function () {
            $.ajax({
                url: "subcategory/brand",
                type: "POST",
                data: {
                    'id': $(this).val(),
                },
                success: function (data) {
                    var result = "";
                            $.each(data, function (k, v) {
                                result += "<option value='"+v['id']+"'>"+v['name']+"</option>";
                            });
                           $('#brand_id').html('');
                           $('#brand_id').html(result);
                           
                    console.log(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        });
        
    });
</script>
@endsection


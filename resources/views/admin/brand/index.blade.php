@extends('admin.master')
@section('breadcrumb')
<li class="active">Dashboard</li>
@endsection
@section('page-header')
Category
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Category 
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-7">
        <table id="simple-table" class="table  table-hover panel panel-default">
            <thead>
                <tr>
                    <th>Logo</th>
                    <th class="detail-col">Category Name</th>
                    <th class="">Status</th>
                    <th style="text-align: right">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($brands as $brand)
                <tr>

                    <td><img src="{{asset('asset/admin/images/brand/'.$brand->logo)}}" alt="Logo" width="40" height="40"/></td>
                    <td>{{$brand->name}}</td>
                    

            <td>
                {!! Form::open(['method'=>'PUT','url'=>'/states']) !!}
                @php($item = $brand)
                <input type="hidden" name="table" value="brands">
                <input type="hidden" name="id" value="{{$item->id}}">
                <input type="hidden" name="states" value="{{$item->states==1?0:1}}">
                <button type="submit" class="btn btn-xs {{$item->states==1?'btn-success':'btn-danger'}}">
                    <i class="ace-icon fa {{$item->states==1?'fa-check':'fa-close'}} bigger-120"></i>
                </button>&nbsp;&nbsp;{{$item->states==1?'Published':'Unpublished'}}
                {!! Form::close() !!}   
            </td>
            
            <td style="text-align: right">
                <div class="hidden-sm hidden-xs btn-group">
                    <button class="btn btn-xs btn-info">
                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                    </button>

                    <button class="btn btn-xs btn-danger">
                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    </button>
                </div>

                <div class="hidden-md hidden-lg">
                    <div class="inline pos-rel">
                        <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                            <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

                            <li>
                                <a href="#" class="tooltip-info" data-rel="tooltip" title="Edit">
                                    <span class="blue">
                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                    </span>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                                    <span class="green">
                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </td>
            </tr>
            @endforeach

            </tbody>
        </table>

        <div>
            {!! $brands->render() !!}
        </div>       



    </div>
    <div class="col-xs-12 col-sm-offset-1 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Category
            </div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','url'=>'/brand_store','files'=>true]) !!}
                <input type="text" name="name" id="name" placeholder="Brand Name" class="form-control" />

                <br/>
                <textarea name="description" placeholder="Description" class="form-control"></textarea>
                <br/>
                <input type="file" name="logo" class="form-control">
                <br/>
                <label>
                    <input type="checkbox" name="states" value="1" id="id-file-format" class="ace" />
                    <span class="lbl"> Published</span>
                </label>
                <br/>
                <br/>
                <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Reset
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>

@endsection
@section('js')
<script type="text/javascript">

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
@endsection

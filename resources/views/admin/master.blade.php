<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <meta name="csrf-token" content="<?php echo csrf_token() ?>">
        <title>Dashboard - Ace Admin</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="{{asset('asset/admin/css/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{asset('asset/admin/font-awesome/4.5.0/css/font-awesome.min.css')}}" />

        <!-- page specific plugin styles -->

        <!-- text fonts -->
        <link rel="stylesheet" href="{{asset('asset/admin/css/fonts.googleapis.com.css')}}" />

        <!-- ace styles -->
        <link rel="stylesheet" href="{{asset('asset/admin/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

        <!--[if lte IE 9]>
                <link rel="stylesheet" href="{{asset('asset/admin/css/ace-part2.min.css')}}" class="ace-main-stylesheet" />
        <![endif]-->
        <link rel="stylesheet" href="{{asset('asset/admin/css/ace-skins.min.css')}}" />
        <link rel="stylesheet" href="{{asset('asset/admin/css/ace-rtl.min.css')}}" />
        <link rel="stylesheet" href="{{asset('asset/admin/custom.css')}}" />
        <link href="{{asset('asset/plagin/chosen/chosen.min.css')}}" rel="stylesheet" type="text/css"/>
        
        @yield('css')
        <!--[if lte IE 9]>
          <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
        <![endif]-->

        <!-- inline styles related to this page -->

        <!-- ace settings handler -->
        <script src="{{asset('asset/admin/js/ace-extra.min.js')}}"></script>
<link href="{{asset('asset/plagin/file_upload/dropzone.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('asset/plagin/file_upload/basic.css')}}" rel="stylesheet" type="text/css"/>
        <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

        <!--[if lte IE 8]>
        <script src="{{asset('asset/admin/js/html5shiv.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/respond.min.js')}}"></script>
        <![endif]-->
    </head>

    <body class="no-skin">
        @include('admin.partial._nav')

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
try {
    ace.settings.loadState('main-container')
} catch (e) {
}
            </script>

            @include('admin.partial._sidbar')	

            <div class="main-content">
                <div class="main-content-inner">

                    @include('admin.partial._breadcrumb')	
                    <div class="page-content">
                        @include('admin.partial._settings_container')
                        <div class="page-header">
                            <h1>
                                @yield('page-header')
                            </h1>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->
                                @include('admin.partial._msg')	

                                 @yield('content')

                                <!-- PAGE CONTENT ENDS -->
                            </div><!-- /.col -->
                        </div>
                       
                        <!-- /.row -->
                    </div><!-- /.page-content -->
                </div>
            </div><!-- /.main-content -->

            @include('admin.partial._footer')		

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="{{asset('asset/admin/js/jquery-2.1.4.min.js')}}"></script>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
        <!-- <![endif]-->

        <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
        <script type="text/javascript">
if ('ontouchstart' in document.documentElement)
    document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>
        <script src="{{asset('asset/admin/js/bootstrap.min.js')}}"></script>
        <!-- page specific plugin scripts -->

        <!--[if lte IE 8]>
          <script src="assets/js/excanvas.min.js"></script>
        <![endif]-->
        <script src="{{asset('asset/admin/js/jquery-ui.custom.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/jquery.ui.touch-punch.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/jquery.easypiechart.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/jquery.sparkline.index.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/jquery.flot.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/jquery.flot.pie.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/jquery.flot.resize.min.js')}}"></script>

        <!-- ace scripts -->
        <script src="{{asset('asset/admin/js/ace-elements.min.js')}}"></script>
        <script src="{{asset('asset/admin/js/ace.min.js')}}"></script>
        <script src="{{asset('asset/plagin/chosen/chosen.jquery.min.js')}}" type="text/javascript"></script>
        <!-- inline scripts related to this page -->
        <script src="{{asset('asset/plagin/file_upload/dropzone.js')}}" type="text/javascript"></script>
      @yield('js')
    </body>
</html>

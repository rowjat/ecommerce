<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Bootstrap Snippet: Login Form</title>


        <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>


        <link href="{{asset('asset/admin/login.css')}}" rel="stylesheet" type="text/css"/>

    </head>

    <body>
        <div class="wrapper">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>
                    {{ $message }}
                </p>
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning">
                <p>
                    {{ $message }}
                </p>
            </div>
            @endif
            <form class="form-signin" role="form" method="POST" action="{{ url('/auth/register') }}">
                {{ csrf_field() }}
                <h2 class="form-signin-heading">Please Register</h2>
                <input id="name" type="text" class="form-control t" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
                <input id="email" type="email" class="form-control c" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
                <input id="password" type="password" class="form-control c" name="password" placeholder="Password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                <input id="password-confirm" type="password" class="form-control b" name="password_confirmation" placeholder="Confirm Password" required>                          

                <button class="btn btn-primary btn-block login" type="submit">Register</button>   
            </form>
        </div>

    </body>
</html>


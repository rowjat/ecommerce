<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Bootstrap Snippet: Login Form</title>


        <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>


        <link href="{{asset('asset/admin/login.css')}}" rel="stylesheet" type="text/css"/>

    </head>

    <body>
        <div class="wrapper">

            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>
                    {{ $message }}
                </p>
            </div>
            @endif
            @if ($message = Session::get('warning'))
            <div class="alert alert-warning">
                <p>
                    {{ $message }}
                </p>
            </div>
            @endif
            <form class="form-signin" role="form" method="POST" action="{{ url('/auth/login') }}">
                {{ csrf_field() }}
                <h2 class="form-signin-heading">Please login</h2>
                <input id="email" type="email" class="form-control t" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required autofocus>
                @if ($errors->has('email'))
                <span class="alert-danger">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif 
                <input id="password" type="password" class="form-control b" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                <span class="alert-danger" >
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
                <label class="checkbox">
                    <input type="checkbox" {{ old('remember') ? 'checked' : ''}} >Remember me
                </label>
                <button class="btn btn-primary btn-block login" type="submit">Login</button>   
            </form>
        </div>

    </body>
</html>

@extends('admin.master')
@section('breadcrumb')
<li class="active">Dashboard</li>
@endsection
@section('page-header')
Product
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Product 
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 ">
        <a href="{{url('/product')}}" class="btn btn-xs btn-info"><i class="ace-icon fa fa-backward bigger-110"></i>Back</a>
    </div>
</div>
<div id="adv-custom-caption" class="center"></div>
<br/>
<div class="row">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <div class="panel panel-info">
            <div class="panel-heading">Add Product</div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','url'=>'/stock_in']) !!}
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-0">
                        <label class="control-label" for="date">Date :</label>
                        <input type="date" name="date" value="{{date('Y-m-d')}}" id="date" class="form-control" placeholder="Date">
                        <br/>  
                        <label class="control-label" for="supplier_id">Company Name :</label>
                        <select type="text" name="supplier_id" id="supplier_id" class="form-control">
                            <option disabled selected>Select </option>
                            @foreach($suppliars as $suppliar)
                            <option value="{{$suppliar->id}}">{{$suppliar->company_name}}</option>
                            @endforeach
                        </select>
                        <br/> 
                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-offset-0">
                        <label class="control-label" for="challana_no">Challan No :</label>
                        <input type="text" name="challana_no" id="challana_no" class="form-control" placeholder="Challan No">
                        <br/> 
                        <label class="control-label" for="supplier_price">Saplier Unit Price :</label>
                        <input type="number" name="supplier_price" id="supplier_price" class="form-control" placeholder="Saplier Unit Price">
                        <br/>
                    </div>
                </div>




                <table id="simple-table" class="table  table-hover panel panel-default">
                    <thead>
                        <tr>
                            <th class="detail-col">Id</th>
                            <th class="">Size</th>
                            <th class="">Color</th>
                            <th class="">Add Qty</th>
                            <th style="text-align: right">Qty</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($products as $product)
                        @foreach(App\Product_deatils::where('product_color_id',$product->id)->get() as $value)
                        <tr>
                            <td>{{$value->id}}</td><input type="hidden" name="product_details_id[]"  value="{{$value->id}}">
                    <td><span class="btn btn-sm btn-default">{{$value->size->name}}</span></td>
                    <td>
                        <span class="badge" style="background: {{\App\Color::find($product->color_id)->code}}"> {{\App\Color::find($product->color_id)->name}}</span>
                    </td>
                    <td class="form-group-sm" style="text-align: right;width: 100px">
                        <input type="number" name="qty[]" class="form-control" value="0" placeholder="Add Qty">
                    </td>
                    <td style="text-align: right;width: 100px">{{$value->qty}}</td>

                    </tr>
                    @endforeach
                    @endforeach

                    </tbody>
                </table>


                <div>
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                    &nbsp; &nbsp; &nbsp;
                    <a href="{{url('/product')}}" class="btn btn-danger" type="reset" >
                        <i class="ace-icon fa fa-close bigger-110"></i>
                        Cancle
                    </a>
                    {!! Form::close() !!}
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    //
</script>
@endsection


@extends('admin.master')
@section('breadcrumb')
<li class=""><a href="">Dashboard</a></li>
<li class="active">Category</li>
@endsection
@section('page-header')
Size
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Size
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <a href="{{url('/supplier_create')}}" class="btn btn-xs btn-primary">Create Supplier</a>
        
    </div>
</div><br/>
<div class="row">
    <div class="col-xs-12 col-sm-offset-1 col-sm-5">
        <table id="simple-table" class="table  table-hover panel panel-default">
            <thead>
                <tr>
                    <th class="detail-col">Size Name</th>
                    <th style="text-align: right">Action</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td></td>
                    <td style="text-align: right">
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>

                            <button class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto" >
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                   
                                    <li>
                                        <a href="#" class="tooltip-info" data-rel="tooltip" title="Edit">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                                            <span class="green">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>

        <div>
           <!--$categories->render()--> 
        </div>

       

    </div>

</div>


@endsection




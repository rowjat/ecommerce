@extends('admin.master')
@section('breadcrumb')
<li class=""><a href="">Dashboard</a></li>
<li class="active">Supplier</li>
@endsection
@section('page-header')
Supplier
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Supplier Inpormation form.
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <a href="{{url('/supplier')}}" class="btn btn-xs btn-default"><i class="fa fa-backward"></i>&nbsp;Back</a>

    </div>
</div><br/>
<div class="row">
    {!! Form::open(['method'=>'POST','url'=>'/supplier_store','files'=>true]) !!}
    <div class="col-xs-12 col-sm-offset-1 col-sm-5">
        
        <input type="text" name="company_name" class="form-control" placeholder="Company Name">
        <br/>
        <textarea name="address" class="form-control" placeholder="Address" rows="4"></textarea>
        <br/>
        <input type="text" name="city" class="form-control" placeholder="City">
        <br/>
        
        
        
    </div>
    <div class="col-xs-12 col-sm-5">
        <input type="text" name="state" class="form-control" placeholder="State">
        <br/>
        <input type="text" name="contry" class="form-control" placeholder="Contry">
        <br/>
        <input type="text" name="phone" class="form-control" placeholder="Phone No">
        <br/>
        <input type="email" name="email" class="form-control" placeholder="Email">
        <br/>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
            <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
        </button>

        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            Reset
        </button>
        &nbsp; &nbsp; &nbsp;
        <button class="btn" >
            <i class="ace-icon fa fa-close bigger-110"></i>
            Cancle
        </button>
        </div> 
    </div>
{!! Form::close() !!}
</div>


@endsection






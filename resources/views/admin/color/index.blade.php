@extends('admin.master')
@section('breadcrumb')
<li class=""><a href="">Dashboard</a></li>
<li class="active">Category</li>
@endsection
@section('page-header')
Color
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Color
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-7">
        <table id="simple-table" class="table  table-hover panel panel-default">
            <thead>
                <tr>
                    <th class="detail-col">Color</th>
                    <th class="">color code</th>
                    <th class="detail-col">Color Name</th>
                    <th style="text-align: right">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($colors as $color)
                <tr>
                    <td>
                            <i class="ace-icon fa  fa-square bigger-120" data-rel="tooltip" title="{{$color->name}}" style="color: {{$color->code}}"></i>
                        
                    </td>
                    <td>{{$color->name}}</td>
                    <td>{{$color->code}}</td>

                    <td style="text-align: right">
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>

                            <button class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto" >
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                   
                                    <li>
                                        <a href="#" class="tooltip-info" data-rel="tooltip" title="Edit">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                                            <span class="green">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        <div>
           <!--$categories->render()--> 
        </div>

       

    </div>
    <div class="col-xs-12 col-sm-offset-1 col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add Color
            </div>
            <div class="panel-body">
                {!! Form::open(['method'=>'POST','url'=>'/color_store']) !!}
                <input type="text" name="name" id="name" placeholder="Color Name" class="form-control" />
                <br/>
                <input type="color" name="code" id="code" placeholder="Select Color" class="form-control">
                <br/>
                <br/>
                <button class="btn btn-info" type="submit">
                    <i class="ace-icon fa fa-check bigger-110"></i>
                    Submit
                </button>

                &nbsp; &nbsp; &nbsp;
                <button class="btn" type="reset">
                    <i class="ace-icon fa fa-undo bigger-110"></i>
                    Reset
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>


@endsection




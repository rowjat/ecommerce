@extends('admin.master')
@section('breadcrumb')
<li class="active">Dashboard</li>
@endsection
@section('page-header')
Product
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Product 
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 ">
        <a href="{{url('product.create')}}" class="btn btn-xs btn-primary">Create Product</a>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-sm-offset-0">
        <table id="simple-table" class="table  table-hover panel panel-default">
            <thead>
                <tr>
                    <th class="detail-col">Name</th>
                    <th class="">Code</th>
                    <th class="">Model</th>
                    <th class="">Category</th>
                    <th class="">Sub Category</th>
                    <th class="">Brand</th>
                    <th class="">States</th>
                    <th style="text-align: right">Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($products as $product)
                <tr>

                    <td>{{$product->name}}</td>
                    <td >{{$product->code}}</td>
                    <td >{{$product->model_no}}</td>
                    <td >{{$product->category->name}}</td>
                    <td >{{$product->subCategory?$product->subCategory->name:'N/A'}}</td>
                    <td >{{$product->brand->name}}</td>
                    <td >
                        {!! Form::open(['method'=>'PUT','url'=>'/states']) !!}
                        @php($item = $product)
                        <input type="hidden" name="table" value="products">
                        <input type="hidden" name="id" value="{{$item->id}}">
                        <input type="hidden" name="states" value="{{$item->states==1?0:1}}">
                        <button type="submit" class="btn btn-xs {{$item->states==1?'btn-success':'btn-danger'}}">
                            <i class="ace-icon fa {{$item->states==1?'fa-check':'fa-close'}} bigger-120"></i>
                        </button>&nbsp;&nbsp;{{$item->states==1?'Published':'Unpublished'}}
                        {!! Form::close() !!}
                        

                    <td style="text-align: right">
                        <div class="hidden-sm hidden-xs btn-group">
                            <button class="btn btn-xs btn-info">
                                <a href="{{url('/product',$product->id)}}"> <i class="ace-icon fa fa-plus bigger-120" style="color: white"> Product </i></a>
                            </button>
                            <button class="btn btn-xs btn-info">
                                <a href="{{url('/pictur',$product->id)}}"> <i class="ace-icon fa fa-picture-o bigger-120" style="color: white"> Pictur </i></a>
                            </button>
                            <button class="btn btn-xs btn-info">
                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                            </button>

                            <button class="btn btn-xs btn-danger">
                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                            </button>
                        </div>

                        <div class="hidden-md hidden-lg">
                            <div class="inline pos-rel">
                                <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto" >
                                    <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                                </button>

                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">

                                    <li>
                                        <a href="#" class="tooltip-info" data-rel="tooltip" title="Edit">
                                            <span class="blue">
                                                <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#" class="tooltip-success" data-rel="tooltip" title="Delete">
                                            <span class="green">
                                                <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>

        <div>
            <!--$categories->render()--> 
        </div>



    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function ()
    {
        
    });
</script>
@endsection


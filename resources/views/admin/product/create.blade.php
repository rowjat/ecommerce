@extends('admin.master')
@section('breadcrumb')
<li class="active">Dashboard</li>
@endsection
@section('page-header')
Product
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Product 
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-offset-1 col-sm-5">
        {!! Form::open(['method'=>'POST','url'=>'/product_store']) !!}
        <br/>
        <select class="form-control" id="category_id" name="category_id" >
            <option value="" disabled selected>Select Category</option>
            @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
            @endforeach
        </select>

        <br/>
        <select class="form-control" id="brand_id" name="brand_id" >
            <option value="" disabled selected>Select Brand</option>
        </select>
        <br/>
        <input type="text" name="name" id="name" class="form-control" placeholder="Product Name">
        <br/>


        <input type="number" name="qty_per_unit" id="qty_per_unit" class="form-control" placeholder="Quantity Per Unit">
        <br/>
        <div class="row">
            <div class="col-xs-6 col-sm-6">
                <input type="number" name="old_unit_price" id="old_unit_price" class="form-control" placeholder="Old Unit Price">
            </div>
            <div class="col-xs-6 col-sm-6">
                <input type="number" name="unit_price" id="unit_price" class="form-control" placeholder="New Unit Price">
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-xs-1 col-sm-1">
                <label>
                    <input type="checkbox" name="states" value="1" class="ace select_size_color" data="size_id" />
                    <span class="lbl"></span>
                </label>
            </div>
            <div class="col-xs-5 col-sm-5">
                <select class="form-control" name="size_id[]" id="size_id" multiple="" disabled="">
                    <!--<option value="0">None</option>-->
                    @foreach($sizes as $size)
                    <option value="{{$size->id}}">{{$size->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-xs-1 col-sm-1">
                <label>
                    <input type="checkbox" name="states" value="1"  class="ace select_size_color" data="color_id" />
                    <span class="lbl"></span>
                </label>
            </div>
            <div class="col-xs-5 col-sm-5">
                <select class="form-control" name="color_id[]" id="color_id" multiple="" disabled>
                    @foreach($colors as $color)
                    <option value="{{$color->id}}" style="background:{{$color->code}}; ">{{$color->name}}</option>
                    @endforeach
                </select> 
            </div>
        </div>

        <br/>
    </div>
    <div class="col-xs-12  col-sm-5">
        <br/>
        <select class="form-control" name="sub_category_id" id="sub_category_id" disabled="" required="">

            <option disabled selected value="">Select Sub_Category</option>

        </select> 
        <br/>
        <input type="text" name="model_no" id="model_no" class="form-control" placeholder="Model No">
        <br/>
        <input type="text" name="code" id="code" class="form-control" placeholder="Product Code">
        <br/>
        <textarea  name="short_description" id="short_description" class="form-control" placeholder="Short Description"></textarea>
        <br/>
        <textarea name="long_description" id="long_description" class="form-control" placeholder="Long Description"></textarea>


        <br/>

        <label>
            <input type="checkbox" name="states" value="1" id="id-file-format" class="ace" />
            <span class="lbl"> Published</span>
        </label>
        <br/>


    </div>


</div>
<div class="row">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10">
        <button class="btn btn-info" type="submit">
            <i class="ace-icon fa fa-check bigger-110"></i>
            Submit
        </button>

        &nbsp; &nbsp; &nbsp;
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            Reset
        </button>
        {!! Form::close() !!}
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">

    $(document).ready(function () {
//        $("#color_id").chosen({
//            disable_search_threshold: 10
//        });
//        $("#size_id").chosen({
//            disable_search_threshold: 10
//        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $(document).on('click', '.select_size_color', function () {
            var id = $(this).attr('data');
            if (this.checked) {
                $("#"+id).removeAttr('disabled');
//                console.log($("#color_id").attr('id'));
            }else{
                $("#"+id).attr('disabled','');
            }
        });
        $('#category_id').on('change', function () {
            $.ajax({
                url: "product_ajaxAction",
                type: "POST",
//                async: false,
                data: {
                    'id': $('#category_id').val(),
                    'i': 'category'
                },
                success: function (data) {

                    var brand_result = "<option value='' disabled selected>Select Brand</option>";
                    var sub_category_result = "<option value='' selected>Select Sub_Category</option>";
                    $("#sub_category_id").attr('disabled', '');
                    if (data['subCategories'] == 1) {

                        $("#sub_category_id").html(sub_category_result);
                        console.log(1);
                    } else {
                        $("#sub_category_id").removeAttr('disabled');

                        $.each(data['subCategories'], function (k, v) {
                            sub_category_result += "<option value='" + v.id + "'>" + v.name + "</option>";
                        });
                        $("#sub_category_id").html('');
                        $("#sub_category_id").html(sub_category_result);
                        console.log(sub_category_result);
                    }

                    $.each(data['brand'], function (k, v) {
                        brand_result += "<option value='" + v.id + "'>" + v.name + "</option>";
                    });

                    $("#brand_id").append().empty();
                    $("#brand_id").append(brand_result);
                    console.log('Error:', data['subCategories']);
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        });
    });
</script>
@endsection


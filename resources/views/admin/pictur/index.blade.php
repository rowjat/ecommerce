@extends('admin.master')
@section('breadcrumb')
<li class="active">Dashboard</li>
@endsection
@section('page-header')
Product
<small>
    <i class="ace-icon fa fa-angle-double-right"></i>
    Add Product 
</small>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 ">
        <a href="{{url('/product')}}" class="btn btn-xs btn-info"><i class="fa fa-backward"></i>&nbsp;Back</a>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-xs-12 col-sm-4 col-sm-offset-0 form-inline" style="border-right: dotted 2px #EEE">
        <table>
            <tr>
                <th style="text-align: right">Product Code :</th>
                <td> &nbsp;{{$products->code}}</td>
            </tr>
            <tr>
                <th style="text-align: right">Model No :</th>
                <td> &nbsp;{{$products->model_no}}</td>
            </tr>
            <tr>
                <th style="text-align: right">Product Name :</th>
                <td> &nbsp;{{$products->name}}</td>
            </tr>
            <tr>
                <th style="text-align: right">Category Name :</th>
                <td> &nbsp;{{$products->category->name}}</td>
            </tr>
            @if($products->subCategory)
            <tr>
                <th style="text-align: right">Sub_category Name :</th>
                <td> &nbsp;{{$products->subCategory->name}}</td>
            </tr>
            @endif
            <tr>
                <th style="text-align: right">Brand Name :</th>
                <td> &nbsp;{{$products->brand->name}}</td>
            </tr>




        </table>


    </div>
    <div class="col-xs-12 col-sm-8 col-sm-offset-0" >
        <div class="row show_pictur"  >

        </div>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-offset-0">

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>No of Pictur</th>
                    <th>Uplode</th>
                    <th>Action</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                @foreach($product_colors as $product)
                {!! Form::open(['method'=>'POST','url'=>'/pictur.store','files'=>true]) !!}
                <tr>
                    <td><span class="badge" style="background: {{\App\Color::find($product->color_id)->code}}">{{\App\Color::find($product->color_id)->name}}</span></td>
                    <td>{{\App\Pictur::where('product_color_id',$product->id)->count()}}</td>
                    <td>
                        <input type="hidden" name="id" value="{{$product->id}}">
                        <input type="hidden" name="product_id" value="{{$product->product_id}}">
                        <input type="file" name="pictur[]" multiple="" />

                    </td>
                    <td>
                        <button type="submit" class="btn btn-xs btn-success">Upload</button>
                    </td>
                    <td >
                        <!--<button class="btn btn-xs btn-info">-->
                        <i class="ace-icon fa fa-image bigger-120 view-pic" style="cursor: pointer" id="{{$product->id}}"></i>
                        <!--</button>-->


                    </td>
                </tr>
                {!! Form::close() !!}
                @endforeach
            </tbody>
        </table>

    </div>

    <div class="col-xs-12 col-sm-6 col-sm-offset-0">
        <div class="">



        </div> 
    </div>

</div>
@endsection
@section('js')
<script>
    $(document).ready(function ()

    {
        $(document).on('change', '.select-color', function () {
            var id = $(this).val();
            div = '<div id="' + id + '"></div>';
            $('.selectfile').append().empty();
            $('.selectfile').append(div);


            console.log(id);
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('mouseover', '.view-pic', function () {
            $.ajax({
                url: "{{url('/pictur_viewPictur')}}",
                type: "POST",
//                async: false,
                data: {
                    'id': $(this).attr('id')
                },
                success: function (data) {

                    $(".show_pictur").append().empty();
                    $(".show_pictur").append(data);
                    console.log(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        });

    });
</script>
@endsection



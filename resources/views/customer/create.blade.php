@extends('master')
@section('content')

<section id="advertisement">
    <div class="container">
        <img src="images/shop/advertisement.jpg" alt="" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
@if(Session::has('success')) 
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
    </button>

    <i class="ace-icon fa fa-check green"></i>


    {{Session::get('success')}}
</div>
@endif
            <section id="form"><!--form-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <div class="login-form"><!--login form-->
                                <h2>Login to your account</h2>
                                {!! Form::open(['method'=>'POST','url'=>'/customer/login']) !!}
                                <input type="email" name="email_1" placeholder="Email Address" />
                                @if ($errors->has('email_1'))
                                <span class="help-block alert-danger">
                                    <strong>{{ $errors->first('email_1') }}</strong>
                                </span>
                                @endif
                                <input type="password" name="password_1" placeholder="Email Address" />
                                @if ($errors->has('password_1'))
                                <span class="help-block alert-danger">
                                    <strong>{{ $errors->first('password_1') }}</strong>
                                </span>
                                @endif
                                <span>
                                    <input type="checkbox" class="checkbox"> 
                                    Keep me signed in
                                </span>
                                <button type="submit" class="btn btn-default">Login</button>
                                {!! Form::close() !!}
                            </div><!--/login form-->
                        </div>
                        <div class="col-sm-1">
                            <h2 class="or">OR</h2>
                        </div>
                        <div class="col-sm-4">
                            <div class="signup-form"><!--sign up form-->
                                <h2>New User Signup!</h2>
                                {!! Form::open(['method'=>'POST','url'=>'/customer/register']) !!}


                                <input type="text" name="name" placeholder="Name" required=""/>
                                @if ($errors->has('name'))
                                <span class="help-block alert-danger">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                                <input type="email" name="email" placeholder="Email Address"/>
                                 @if ($errors->has('email'))
                                <span class="help-block alert-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                                <input id="password" type="password"  name="password" placeholder="Password" >

                                
                                 @if ($errors->has('password'))
                                <span class="help-block alert-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                                <input id="password-confirm" type="password"  name="password_confirmation" placeholder="Confirm Password">
                                <button type="submit" class="btn btn-default">Signup</button>
                                {!! Form::close() !!}
                            </div><!--/sign up form-->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>
@endsection
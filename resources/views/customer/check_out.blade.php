@extends('master')
@section('content')

<section>
    <div class="container">
        <div class="row">
            @if(Session::has('success')) 
            <div class="alert alert-block alert-success">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>

                <i class="ace-icon fa fa-check green"></i>


                {{Session::get('success')}}
            </div>
            @endif
            <section id="cart_items">
                <div class="shopper-informations">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="review-payment">
                                <h2>Shipping Details</h2>
                            </div>
                            <div class="shopper-info">

                                {!! Form::open(['method'=>'POST','url'=>'/order']) !!}

                                
                                    <input type="text" name="mobile" placeholder="Mobile">
                                    <input type="text" name="country" placeholder="Country">
                                    <input type="text" name="city" placeholder="City">
                                    <input type="text" name="state" placeholder="State">
                                    <textarea name="address" placeholder="Address" rows="3"></textarea>
                                    <br/>
                                    <br/>
                                    <label for="payment_type" class="control-label">
                                        <input type="radio" class="form-check-input" name="payment_type" id="optionsRadios2" value=1> 
                                        <span>Cash in hand</span>
                                    </label>
                                    <label for="payment_type" class="control-label">
                                        <input type="radio" class="form-check-input" name="payment_type" id="optionsRadios2" value=2> 
                                        <span>Paypal</span>
                                    </label>
                                     
                                    <br/>
                               
                                <button type="submit" class="btn btn-primary" href="">Continue</button>
                                
                            {!! Form::close() !!}
                            <br/>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="review-payment">
                                <h2>Cart</h2>
                            </div>
                            <div class="table-responsive cart_info">
                                 <table class="table table-condensed ">
                                     <thead>
                                <tr class="cart_menu">
                                    <td class="image">Pictur</td>
                                    <td class="image">Item</td>
                                    <td class="description">Size</td>
                                    <td class="description">color</td>
                                    <td class="price">Price</td>
                                    <td class="price">Tax</td>
                                    <td class="quantity" style="width: 150px">Quantity</td>
                                    <td class="total">Total</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody >
                                @php($i =0)
                                @foreach($carts as $cart)
                                @php($i++)
                                <tr>
                                    <td class="">
                                        <?php
                                        $pictur =\App\pictur::hasPictur($cart->id)->where('states',1)->first()->pictur_name;
                                        ?>
                                        <a href=""><img src="{{asset('asset/admin/images/product/'.$pictur)}}" alt="" width="100" height="100"></a>
                                    </td>
                                    <td class="">
                                        <h4><a href="">{{$cart->name}}</a></h4>
                                        <p>Web ID: {{$cart->rowId}}</p>
                                    </td>
                                    <td class="">
                                        <h4><a href="">{{$cart->options->size}}</a></h4>
                                    </td>
                                    <td class="">
                                        <h4><a href="">{{$cart->options->color}}</a></h4>
                                    </td>
                                    <td class="">
                                        <p>BDT {{$cart->price}}</p>
                                    </td>
                                    <td class="">
                                        <p>BDT {{$cart->tax}}</p>
                                    </td>
                                    {!! Form::open(['method'=>'PUT','route'=>['cart.update',$cart->rowId]]) !!}
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">

                                            <a class="cart_quantity_up btn" style="cursor: pointer" id="{{$cart->rowId}}"> + </a>
                                            <input class="cart_quantity_input" min="1" id="input{{$cart->rowId}}" type="text" name="quantity" value="{{$cart->qty}}" autocomplete="off" size="2">
                                            <a class="cart_quantity_down btn" style="cursor: pointer" id="{{$cart->rowId}}"> - </a>

                                        </div>
                                    </td>
                                    <td class="" style="font-size: 10px">
                                        <p class="cart_total_price" style="font-size: 14px">BDT {{$cart->total}}</p>
                                    </td>
                                    <td>
                                        <button style="float: right" type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>
                                    </td>
                                    {!! Form::close() !!}
                                    <td>
                                        {!! Form::open(['method'=>'DELETE','route'=>['cart.destroy',$cart->rowId]]) !!}
                                        <button style="float: left" type="submit" class="btn btn-sm btn-default"><i class="fa fa-times"></i></button>
                                        {!! Form::close() !!}  
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                            <td colspan="6">&nbsp;</td>
                                            <td colspan="4">
                                                <table class="table table-condensed total-result">
                                                    <tr>
                                                        <td>Cart Sub Total</td>
                                                        <td>{{Cart::subtotal()}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Exo Tax</td>
                                                        <td>{{Cart::tax()}}</td>
                                                    </tr>
                                                    <tr class="shipping-cost">
                                                        <td>Shipping Cost</td>
                                                        <td>Free</td>										
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td><span>{{ Cart::total()}}</span></td>
                                                    </tr>
                                                    
                                                </table>
                                            </td>
                                        </tr>
                            </tbody>
                        </table>
                            </div>
                        </div>				
                    </div>
                </div>

            </section>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    $(document).ready(function () {
        $(document).on('click', '.cart_quantity_up', function () {
            var id = $(this).attr('id');
            var current_qty = parseInt($('#input' + id).val());
            var input_val = $('#input' + id).attr('value', current_qty + 1);
            console.log(id);
        });
        $(document).on('click', '.cart_quantity_down', function () {
            var id = $(this).attr('id');
            var current_qty = parseInt($('#input' + id).val());
            if (current_qty > 1) {
                $('#input' + id).attr('value', current_qty - 1);
            }
        });

    });
</script>
@endsection

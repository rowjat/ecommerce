<header id="header"><!--header-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <a href="{{url('/')}}"><img src="{{asset('asset/images/home/logo.png')}}" alt="" /></a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="{{route('cart.index')}}"><i class="fa fa-shopping-cart"></i> Cart
                                    @if(Cart::count()>0)
                                    <span class="badge" style="background: #ff9900">{{Cart::count()}}</span>
                                    @endif
                                </a>
                            </li>
                            @if(Auth::check())

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-arrow-circle-up"></span>&nbsp; {{Auth::check()?auth()->user()->name:''}} <span class="caret"></span></a>
                                <ul class="dropdown-menu">

                                    <li><a href="#">Orders</a></li><br/>

                                    <li role="separator" class="divider"></li><br/>
                                    <li><a href="#">Profile</a></li><br/>
                                    <li><a href="{{url('/customer/logout')}}">Logout</a></li><br/>
                                </ul>
                            </li>

                            @else
                            <li><a href="{{url('/order/checkout')}}"><i class="fa fa-lock"></i> Login</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->
</header>


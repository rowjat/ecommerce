<div class="left-sidebar">
    <h2>Category</h2>
    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
        @foreach($categories as $category)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">

                    @if($category->subCategory->count() >0)
                    
                    <a data-toggle="collapse" data-parent="#accordian" href="#{{$category->name}}">
                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                        {{$category->name}}

                    </a>
                    @else
                    <a href="{{url('shop?cat='.$category->id)}}">{{$category->name}}</a>
                    @endif
                </h4>
            </div>
            @if($category->subCategory->count() >0)
            <div id="{{$category->name}}" class="panel-collapse collapse">
                <div class="panel-body">

                    <ul>
                        @foreach($category->subCategory->where('states',1) as $sub)
                        <li><a href="{{url('shop?subCat='.$sub->id)}}">{{$sub->name}} </a></li>
                        @endforeach
                    </ul>

                </div>
            </div>
            @endif

        </div>
        @endforeach

    </div><!--/category-products-->

    <div class="brands_products"><!--brands_products-->
        <h2>Brands</h2>
        <div class="brands-name">
            <ul class="nav nav-pills nav-stacked">
                @foreach($brands as $brand)
                <li><a href="{{url('shop?brand='.$brand->id)}}"><img src="{{asset('/asset/admin/images/brand/'.$brand->logo)}}" alt="" width="15" height="15"/> <span class="pull-right">({{App\Product::where('states',1)->where('brand_id',$brand->id)->count()}})</span>{{$brand->name}}</a></li>
                @endforeach
            </ul>
        </div>
    </div><!--/brands_products-->

    <!--    <div class="price-range">price-range
            <h2>Price Range</h2>
            <div class="well text-center">
                <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="1000" data-slider-step="5" data-slider-value="[0,450]" id="sl2" ><br />
                <b class="pull-left">$ 0</b> <b class="pull-right">$ 1000</b>
            </div>
        </div>/price-range-->

    <div class="shipping text-center">
        <img src="{{asset('asset/images/home/shipping.jpg')}}" alt="" />
    </div>

</div>


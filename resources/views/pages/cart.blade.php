@extends('master')
@section('content')

<section id="cart_items">
    <div class="container">
        <div class="row">
            <div class="table-responsive cart_info">
                <section id="cart_items">
                    <div class="container">
                        @if($carts->count() > 0)
                        <table class="table table-condensed ">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="image">Pictur</td>
                                    <td class="image">Item</td>
                                    <td class="description">Size</td>
                                    <td class="description">color</td>
                                    <td class="price">Price</td>
                                    <td class="price">Tax</td>
                                    <td class="quantity">Quantity</td>
                                    <td class="total">Total</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                @php($i =0)
                                @foreach($carts as $cart)
                                @php($i++)
                                <tr>
                                    <td class="">
                                        <?php
                                        $pictur =\App\pictur::hasPictur($cart->id)->where('states',1)->first()->pictur_name;
                                        ?>
                                        <a href=""><img src="{{asset('asset/admin/images/product/'.$pictur)}}" alt="" width="100" height="100"></a>
                                    </td>
                                    <td class="">
                                        <h4><a href="">{{$cart->name}}</a></h4>
                                        <p>Web ID: {{$cart->rowId}}</p>
                                    </td>
                                    <td class="">
                                        <h4><a href="">{{$cart->options->size}}</a></h4>
                                    </td>
                                    <td class="">
                                        <h4><a href="">{{$cart->options->color}}</a></h4>
                                    </td>
                                    <td class="cart_price">
                                        <p>BDT {{$cart->price}}</p>
                                    </td>
                                    <td class="cart_price">
                                        <p>BDT {{$cart->tax}}</p>
                                    </td>
                                    {!! Form::open(['method'=>'PUT','route'=>['cart.update',$cart->rowId]]) !!}
                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">

                                            <a class="cart_quantity_up btn" style="cursor: pointer" id="{{$cart->rowId}}"> + </a>
                                            <input class="cart_quantity_input" min="1" id="input{{$cart->rowId}}" type="text" name="quantity" value="{{$cart->qty}}" autocomplete="off" size="2">
                                            <a class="cart_quantity_down btn" style="cursor: pointer" id="{{$cart->rowId}}"> - </a>

                                        </div>
                                    </td>
                                    <td class="cart_total">
                                        <p class="cart_total_price">BDT {{$cart->total}}</p>
                                    </td>
                                    <td>
                                        <button style="float: right" type="submit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i></button>
                                    </td>
                                    {!! Form::close() !!}
                                    <td>
                                        {!! Form::open(['method'=>'DELETE','route'=>['cart.destroy',$cart->rowId]]) !!}
                                        <button style="float: left" type="submit" class="btn btn-sm btn-default"><i class="fa fa-times"></i></button>
                                        {!! Form::close() !!}  
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                            <td colspan="6">&nbsp;</td>
                                            <td colspan="4">
                                                <table class="table table-condensed total-result">
                                                    <tr>
                                                        <td>Cart Sub Total</td>
                                                        <td>{{Cart::subtotal()}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Exo Tax</td>
                                                        <td>{{Cart::tax()}}</td>
                                                    </tr>
                                                    <tr class="shipping-cost">
                                                        <td>Shipping Cost</td>
                                                        <td>Free</td>										
                                                    </tr>
                                                    <tr>
                                                        <td>Total</td>
                                                        <td><span>{{ Cart::total()}}</span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"><a class="btn btn-default check_out" href="{{url('/order/checkout')}}">Check Out</a></td>
                                                        
                                                    </tr>
                                                    
                                                </table>
                                            </td>
                                        </tr>
                            </tbody>
                        </table>
                        @else
                        <h3 style="color: red;font-weight: bold">You have no any Item </h3>
                        @endif
                    </div>
                </section>
            </div>
            <div class="col-sm-9 padding-right">

            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    $(document).ready(function () {
        $(document).on('click', '.cart_quantity_up', function () {
            var id = $(this).attr('id');
            var current_qty = parseInt($('#input' + id).val());
            var input_val = $('#input' + id).attr('value', current_qty + 1);
            console.log(id);
        });
        $(document).on('click', '.cart_quantity_down', function () {
            var id = $(this).attr('id');
            var current_qty = parseInt($('#input' + id).val());
            if (current_qty > 1) {
                $('#input' + id).attr('value', current_qty - 1);
            }
        });

    });
</script>
@endsection
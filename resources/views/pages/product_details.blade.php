@extends('master')
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                {!!$sidebar!!}
            </div>
            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="col-sm-5">
                        <div class="view-product" id="pictur">
                            @php($pictur = $product->pictur->where('states',1)->first()?$product->pictur->where('states',1)->first()->pictur_name:'')
                            <img  src="{{asset('asset/admin/images/product/'.$pictur)}}" alt="" />

                            <!--<h3></h3>-->
                        </div>
                        <div id="similar-product" class="carousel slide" data-ride="carousel">

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" style="height: 60px">
                                <div class="item active">
                                    @php($i=0)
                                    @foreach($product->pictur as $v)
                                    @php($i++)
                                    <img style="cursor: pointer" src="{{asset('asset/admin/images/product/'.$v->pictur_name)}}" data="{{$v->id}}" class="pictur" alt="" width="50" height="100">
                                    @if($i%3==0)
                                </div>
                                <div class="item">
                                    @endif
                                    @endforeach
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left item-control" href="#similar-product" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right item-control" href="#similar-product" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                    <div class="col-sm-7">
                        <div class="product-information"><!--/product-information-->
                          
                            <h2>{{$product->name}}</h2>
                            {!! Form::open(['method'=>'POST','route'=>'cart.store','class'=>'']) !!}
                            <span>
                                <span>BDT {{$product->unit_price}}</span>
                                <button type="submit" id="add_cart" class="btn btn-fefault cart" {{!$product->size->where('id',1)->first() || !$product->color->where('id',1)->first()?'disabled':''}} >
                                    <i class="fa fa-shopping-cart"></i>
                                    Add to cart
                                </button>
                            </span>
                            <input type="hidden" name="product_id" value="{{$product->id}}">
                            <p><b>Quantity :</b>&nbsp;&nbsp;<span><input style="height: 30px" type="number" name="qty" value="1" max="{{$product->size->where('id',1)->first() || $product->size->where('id',1)->first()?$product->product_detail->sum('qty'):''}}"></span>&nbsp;&nbsp;
                                @if(!$product->size->where('id',1)->first())
                                <span>
                                    <select name="size_id" class="size_color" product_id='{{$product->id}}' id="size_id" required="">
                                        <option disabled="" selected="">Size</option>
                                        @foreach($product->size as $size)
                                        <option value="{{$size->id}}">{{$size->name}}</option>
                                        @endforeach
                                    </select>
                                </span>

                                &nbsp;&nbsp;
                                @endif
                                @if(!$product->color->where('id',1)->first())
                                <span>
                                    <select name="color_id" class="size_color" product_id='{{$product->id}}' id="color_id" required="">
                                        <option disabled="" selected="">Color</option>
                                        @foreach($product->color as $color)
                                        <option value="{{$color->id}}">{{$color->name}}</option>
                                        @endforeach
                                    </select>
                                </span>
                                @endif
                            </p>

                            @if(!$product->size->where('id',1)->first() || !$product->color->where('id',1)->first())
                            <p id="show_qty">

                            </p>
                            @else

                            <p><b>Availability:</b>{{$product->product_detail->sum('qty')}}</p>

                            @endif
                            <p><b>Brand:</b> {{$product->brand->name}}</p>
                           
                            {!! Form::close() !!}
                        </div><!--/product-information-->
                    </div>
                </div><!--/product-details-->


            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('mouseover', '.pictur', function () {
            $.ajax({
                url: "{{url('/picture/view')}}",
                type: "POST",
                data: {
                    'id': $(this).attr('data')
                },
                success: function (data) {
                    var url = "{{url('asset/admin/images/product/" + data + "')}}"
                    $('#pictur').append().empty();
                    $('#pictur').append(data);

                    console.log(data);
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        });


        $(document).on('change', '.size_color', function () {
            var count = $('.size_color').length;
            var input = 0;
            if ($('#color_id').length > 0) {
                input += $('#color_id').val() > 0 ? 1 : 0;
            }
            if ($('#size_id').length > 0) {
                input += $('#size_id').val() > 0 ? 1 : 0;
            }

            var product_id = $(this).attr('product_id');
            var color_id = '';
            var size_id = '';
            if ($('#color_id').length > 0 && $('#color_id').val() == null) {
                color_id = 0;
            } else if ($('#color_id').length > 0) {
                color_id = $('#color_id').val();
            } else {
                color_id = '';
            }

            if ($('#size_id').length > 0 && $('#size_id').val() == null) {
                size_id = 0;
            } else if ($('#size_id').length > 0) {
                size_id = $('#size_id').val();
            } else {
                size_id = '';
            }
            var select = $(this).attr('id');
            var input_id = $(this).val();
            $.ajax({
                url: "{{url('/qty/view')}}",
                type: "POST",
                data: {
                    'product_id': product_id,
                    'color_id': color_id,
                    'size_id': size_id,
                    'input': input == count ? 1 : 0
                },
                success: function (data) {
                    console.log(data);
                    if (data && data > 0) {
                        $('#add_cart').removeAttr('disabled');
                        $('#show_qty').html('');
                        $('#show_qty').html('<b>Availability:</b> ' + data);
                    } else {
                        $('#add_cart').attr('disabled', '');
                        $('#show_qty').html('');
                        $('#show_qty').html('<b>Availability:</b> Not stock');
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }

            });
        });

    });
</script>
@endsection


@extends('master')
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        {!! Form::open(['method'=>'POST','url'=>['/payment/store',$id],'id'=>'payment-form']) !!}
      
        <div id="charge-error" class="alert alert-danger {{!Session::has('error')?'hidden':''}}">
            {{Session::get('error')}}
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group-sm">
                    <label for="card-number" >Credit Card Number</label>
                    <input type="text" id="card-number" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <div class="form-group-sm">
                    <label for="card-expiry-month" >Expiration Month</label>
                    <input type="text" id="card-expiry-month" class="form-control">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="form-group-sm">
                    <label for="card-expiry-year" >Expiration Year</label>
                    <input type="text" id="card-expiry-year" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group-sm">
                    <label for="card-cvc" >CVC</label>
                    <input type="text" id="card-cvc" class="form-control">
                </div>
            </div>
        </div>
        <button class="btn btn-default" >Submit Payment</button>
        {!! Form::close() !!}
    </div>
</div>


@endsection
@section('js')
<script src="https://js.stripe.com/v2/"></script>
<script>
Stripe.setPublishableKey('pk_test_I2xHFtcLgYCIum5LEGcCiEdY');
var $form = $('#payment-form');
$form.submit(function (event) {
    $('#charge-error').addClass('hidden');
    $form.find('button').prop('disabled', true);
    Stripe.card.createToken({
        number: $('#card-number').val(),
        cvc: $('#card-cvc').val(),
        exp_month: $('#card-expiry-month').val(),
        exp_year: $('#card-expiry-year').val()
    }, stripeResponseHandler);
    return false;
});
function stripeResponseHandler(status, response) {
//    var $form = $('#payment-form');
    if (response.error) { // Problem!
        $('#charge-error').removeClass('hidden');
        $('#charge-error').text(response.error.message);
        $form.find('button').prop('disabled', false);

    } else { // Token was created!
        var token = response.id;
        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
        $form.get(0).submit();

    }
}

</script>
@endsection


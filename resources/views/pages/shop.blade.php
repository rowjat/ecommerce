@extends('master')
@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                
               {!!$sidebar!!}
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Features Items</h2>
                    @foreach($products as $product)
                   <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    @php($pictur =App\pictur::where('product_id',$product->id)->where('states',1)->first())
                                   <img src="{{$pictur?asset('asset/admin/images/product/'.$pictur->pictur_name):''}}" alt=""/>
                                    <h2>${{$product->unit_price}}</h2>
                                    <p>{{$product->name}}</p>
                                    <!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content" >
                                        <a href="{{url('/details',$product->id)}}"><img src="{{$pictur?asset('asset/admin/images/product/'.$pictur->pictur_name):''}}" alt=""/></a>
                                         <a href="{{url('/details',$product->id)}}"><h2>${{$product->unit_price}}</h2></a>
                                        <a href="{{url('/details',$product->id)}}"><p>{{$product->name}}</p></a>
                                             <!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
                                    </div>
                                </div>
                                <!--<img src="{{asset('asset/images/home/new.png')}}" class="new" alt="" />-->
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
<!--                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                 
                    @endforeach
<!--                    <ul class="pagination">
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">&raquo;</a></li>
                    </ul>-->
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>
@endsection
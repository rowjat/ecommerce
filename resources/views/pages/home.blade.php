@extends('master')
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                {!!$sidebar!!}
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Features Items</h2>
                    @foreach($products as $product)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    @php($pictur =App\pictur::where('product_id',$product->id)->where('states',1)->first())
                                    <img src="{{$pictur?asset('asset/admin/images/product/'.$pictur->pictur_name):''}}" alt="" height=""/>
                                    <h2>${{$product->unit_price}}</h2>
                                    <p>{{$product->name}}</p>
                                    <a href="#" class="add-to-cart"></a>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content" >

                                        <a href="{{url('/details',$product->id)}}"><h2>${{$product->unit_price}}</h2></a>
                                        <a href="{{url('/details',$product->id)}}"><p>{{$product->name}}</p></a>
                                             <!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
                                    </div>
                                </div>
                                <!--<img src="{{asset('asset/images/home/new.png')}}" class="new" alt="" />-->
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
<!--                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div><!--features_items-->

                <div class="category-tab"><!--category-tab-->
                    <div class="col-sm-12">
                        <ul class="nav nav-tabs">
                            <!--<li class="active"><a href="#tshirt" data-toggle="tab">T-Shirt</a></li>-->
                            @php($i = 0)
                            @foreach($categories as $category)
                            @php($i++)
                            <li class="{{$i==1?'active':''}}"><a href="{{"#".$category->id}}" data-toggle="tab">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="tab-content">
                        @php($i = 0)
                        @foreach($categories as $category)
                        @php($i++)
                        <div class="tab-pane fade {{$i==1?'active in':''}}" id="{{$category->id}}" >
                            <?php
                            \App\Product::where('category_id', $category->id)->where('states', 1)->get();
                            ?>
                            @foreach(\App\Product::where('category_id',$category->id)->where('states',1)->get() as $product)
                            <div class="col-sm-3">
                                <div class="product-image-wrapper">
                                    <div class="single-products">
                                        <div class="productinfo text-center">
                                            @php($pictur =App\pictur::where('product_id',$product->id)->where('states',1)->first())
                                            <img src="{{$pictur?asset('asset/admin/images/product/'.$pictur->pictur_name):''}}" alt="" height="150"/>
                                            <h2>${{$product->unit_price}}</h2>
                                            <p>{{$product->name}}</p>
                                            <!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
                                        </div>
                                        <div class="product-overlay">
                                            <div class="overlay-content" >

                                                <a href="{{url('/details',$product->id)}}"><h2>${{$product->unit_price}}</h2></a>
                                                <a href="{{url('/details',$product->id)}}"><p>{{$product->name}}</p></a>
                                                     <!--<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>-->
                                            </div>
                                        </div>
                                        <!--<img src="{{asset('asset/images/home/new.png')}}" class="new" alt="" />-->
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                        @endforeach
                    </div>
                </div><!--/category-tab-->

                <!--                <div class="recommended_items">recommended_items
                                    <h2 class="title text-center">recommended items</h2>
                
                                    <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="item active">	
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img src="images/home/recommend1.jpg" alt="" />
                                                                <h2>$56</h2>
                                                                <p>Easy Polo Black Edition</p>
                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                            </div>
                
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img src="images/home/recommend2.jpg" alt="" />
                                                                <h2>$56</h2>
                                                                <p>Easy Polo Black Edition</p>
                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                            </div>
                
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img src="images/home/recommend3.jpg" alt="" />
                                                                <h2>$56</h2>
                                                                <p>Easy Polo Black Edition</p>
                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                            </div>
                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">	
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img src="images/home/recommend1.jpg" alt="" />
                                                                <h2>$56</h2>
                                                                <p>Easy Polo Black Edition</p>
                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                            </div>
                
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img src="images/home/recommend2.jpg" alt="" />
                                                                <h2>$56</h2>
                                                                <p>Easy Polo Black Edition</p>
                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                            </div>
                
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <img src="images/home/recommend3.jpg" alt="" />
                                                                <h2>$56</h2>
                                                                <p>Easy Polo Black Edition</p>
                                                                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                            </div>
                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                            <i class="fa fa-angle-left"></i>
                                        </a>
                                        <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                            <i class="fa fa-angle-right"></i>
                                        </a>			
                                    </div>
                                </div>/recommended_items-->

            </div>
        </div>
    </div>
</section>


@endsection


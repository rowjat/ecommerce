<?php

use Illuminate\Database\Seeder;
use App\Color;
use App\Size;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);
         $this->call(ColorTableSeeder::class);
         $this->call(SizeTableSeeder::class);
         $this->call(RoleTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Size;
class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $table = new size();
        $table->name = 'None';
        $table->deletion_states = 0;
        $table->save();
    }
}

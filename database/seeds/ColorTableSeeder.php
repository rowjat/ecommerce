<?php

use Illuminate\Database\Seeder;
use App\Color;
class ColorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = new Color();
        $table->name = 'None';
        $table->code = 'none';
        $table->deletion_states = 0;
        $table->save();
    }
}

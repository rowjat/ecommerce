<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $super_admin = new Role();
        $super_admin->name = 'super_admin';
        $super_admin->save();
        
        $admin = new Role();
        $admin->name = 'admin';
        $admin->save();
        
        $customer = new Role();
        $customer->name = 'customer';
        $customer->save();
        
        $author = new Role();
        $author->name = 'author';
        $author->save();
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->integer('payment_type');
            $table->integer('shipping_id')->unsigned();
            $table->smallInteger('states');
            $table->date('order_date');
            $table->date('possible_delivery_date');
            $table->date('delivery_date');
            $table->smallInteger('deletion_states')->default(0);
            $table->timestamps();
            
            $table->foreign('customer_id')->references('id')->on('customers');
//            $table->foreign('shipping_id')->references('id')->on('shippings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

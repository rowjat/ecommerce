<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('sub_category_id');
            $table->integer('brand_id')->unsigned();
            $table->string('code')->unique();
            $table->string('name');
            $table->text('short_description');
            $table->text('long_description');
            $table->string('model_no')->unique();
            $table->double('qty_per_unit',15,2);
            $table->double('old_unit_price',15,2);
            $table->double('unit_price',15,2);
            $table->smallInteger('states')->default(0);
            $table->smallInteger('deletion_states')->default(0);
            
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

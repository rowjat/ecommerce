<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePictursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('picturs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_color_id');
            $table->integer('product_id')->unsigned();
            $table->string('pictur_name');
            $table->smallInteger('states')->default(0);
            $table->smallInteger('deletion_states')->default(0);
        
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picturs');
    }
}
